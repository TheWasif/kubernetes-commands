# All Kubenretes Commands!

# Start a kubernetes cluster
minikube start

# Check status of a cluster
minikube status

# Describe a resource
kubectl describe [resource] [resource_name] # resource can be 'pod' or 'node'

# List all nodes
kubectl get nodes # you can also use 'node' or 'no'

# Create a Pod by YAML file
kubectl create -f YAMLFile.yml

# List all Pods
kubectl get pods # you can also use 'po' or 'pod'

# Get Pods in different formats | format_name can be 'josn' or 'yaml'
kubectl get pods -o [format_name]

# Create a Pod directly from CLI without YAML file
kubectl run [pod_name] --image=[image_name] --port=80 --restart=Never

# Port Forwarding
kubectl port-forward [pod_name] [outer_port]:[inner_port] # eg: kubectl port-forward mypod 3000:80
